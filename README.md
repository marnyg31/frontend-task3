Solution for frontend task 3 (web scraper)

This repo contains a NodeJS app used for scraping data from reddit. The script will by default scrape from the homepage of reddit, but it can also scrape from subreddits. The way to specify what subreddit to scrape, is by passing the subreddit name as a command line argument like so: `node index.js askreddit`

The script will scrape data about the reddit posts currently on the front page of reddit or subreddid. The data collected about the posts is its ranking, score and title. Then the data is written both to the console, and also to a file named `reddit.json`
