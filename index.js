const puppeteer = require('puppeteer');
var fs = require('fs');
// const { timeStamp } = require('console');

class scraper {
    async main(subreddit) {
        const { page, browser } = await scraper.initPuppeteer(subreddit);
        const data = await this.scrapeData(page);
        this.printScrapedDataToConsole(data);
        this.writeScrapedDataToFile(data);
        await browser.close();
    }

    static async initPuppeteer(subreddit) {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        const url = subreddit ? 'https://old.reddit.com/r/' + subreddit : 'https://old.reddit.com/';
        await page.goto(url);
        return { page, browser };
    }

    async scrapeData(page) {
        return await page.evaluate(() => {
            let a = Array.from(document.querySelectorAll("div.thing .rank")).map(el => el.textContent);
            let b = Array.from(document.querySelectorAll("div.thing .score.unvoted")).map(el => el.textContent);
            let c = Array.from(document.querySelectorAll("div.thing a.title")).map(el => el.textContent);

            const zip = (arr1, arr2, arr3) => arr1.map((k, i) => ({ rank: k, score: arr2[i], title: arr3[i] }));
            return zip(a, b, c);
        });
    }

    printScrapedDataToConsole(data) {
        data.forEach(el => {
            console.log(`Rank: ${el.rank.padEnd(5)} Score: ${el.score.padEnd(8)} Title: ${el.title}`);
        });
    }

    writeScrapedDataToFile(data) {
        fs.writeFile("reddit.json", JSON.stringify(data), (err) => { err ? console.log(err) : null; });
    }
};

const subreddit = process.argv.slice(2)[0]; //first argument passed on command line
new scraper().main(subreddit);